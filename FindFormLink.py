#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
#Requerido, librería bs4
#Requerido, librería lxml
"""

import sys
from bs4 import BeautifulSoup
import urllib
import threading

#web = urllib.urlopen('http://facebook.com').read()


print """
 FFFF  i           d     FFFF                      L     i        k   
 F                 d     F                         L              k   
 F     i  nnnn  dddd     F     ooo   rrrmmmmmm     L     i  nnnn  k kk
 F     i  n  n d   d     F    o   o  r  m m  m     L     i  n  n  k k 
 FFFF  i  n  n d   d     FFFF o   o  r  m m  m     L     i  n  n  kk  
 F     i  n  n d   d     F    o   o  r  m m  m     L     i  n  n  kkk 
 F     i  n  n d   d     F    o   o  r  m m  m     L     i  n  n  k k 
 F     i  n  n  dddd     F     ooo   r  m m  m     LLLL  i  n  n  k kk
 
 by: BladeH
 For: dragonjar.org
"""

if len(sys.argv) != 2 :
  print "***No a pasado el parámetro necesario!!!\n-Ejemplo de uso: python FindFormLink.py http://webSite.com"
  print '\n'
  sys.exit(0)

try:
  web = urllib.urlopen(sys.argv[1]).read()
except:
  print 'Sitio web no encontrado!!! :('
  print '\n'
  sys.exit(0)


pareso = BeautifulSoup(web,'lxml')

def FormulariosLinks(pareso):

    forms = pareso.find_all('form')

    print '\nCantidad de formulario: ' + str(len(forms)) + '\n'
    contardor = 1

    for form in forms:

        print 'Formulario #' + str(contardor) + ' *****************'
        print ' Action: ' +  unicode(form['action'])
        print ' method: ' +  unicode(form['method'])

        inputs = form.find_all('input')
        print ' Cantidad de inputs:: ' + str(len(inputs))
        for i in inputs:
          try:
            print '     Type: ' + unicode(i['type']) +' Name: ' + unicode(i['name'])
          except:
            print '     Type: ' + unicode(i['type'])

        contardor += 1
        print '\n'

    links = pareso.find_all('a')
    print '\nCantidad de links: ' + str(len(links)) + '\n'
    contardor = 1

    for link in links:

      href = str(link['href'])
      print ' ' + str(contardor) + '-Link href: ' +  unicode(href) + ' Texto: ' +  unicode(link.string)
      contardor += 1

    print '\n'

if __name__ == '__main__':
    threads = list()
    busquedaForms = threading.Thread(target=FormulariosLinks,args=(pareso,))
    threads.append(busquedaForms)
    busquedaForms.start()
